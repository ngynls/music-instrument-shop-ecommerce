<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('products', 'ProductsController');

Route::get('/cart', 'CartController@index')->name('cart.index');
Route::post('/cart', 'CartController@store')->name('cart.store');
Route::delete('/cart/{id}', 'CartController@destroy')->name('cart.destroy');
Route::patch('/cart/{id}', 'CartController@update')->name('cart.update');

Route::get('/results', 'SearchController@search')->name('search');

Route::get('/checkout', 'CheckoutController@index')->name('checkout');

Route::post('/payment', 'CheckoutController@payment')->name('payment');

Route::post('/subscribeToNewsletter','SubscriptionController@subscribe')->name('subscribe');

Route::get('/myAccount','PagesController@myAccount')->name('myAccount');

Route::get('/myOrderHistory','PagesController@myOrderHistory')->name('orderHistory');

Route::get('/orders/{id}','PagesController@viewOrder')->name('viewOrder');
