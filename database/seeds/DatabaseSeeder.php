<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();
        $this->call(CategoriesTableSeeder::class);
        $this->command->info("Categories table seeded!");
        $this->call(ManufacturersTableSeeder::class);
        $this->command->info("Manufacturers table seeded!");
        $this->call(ProductsTableSeeder::class);
        $this->command->info("Product table seeded!");
    }
}
