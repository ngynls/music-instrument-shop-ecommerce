<?php

use App\Manufacturer;
use Illuminate\Database\Seeder;

class ManufacturersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('manufacturers')->delete();
        Manufacturer::create([
            'id'=>1,
            'manuf_name'=>'Fender'
        ]);
        Manufacturer::create([
            'id'=>2,
            'manuf_name'=>'Gibson'
        ]);
        Manufacturer::create([
            'id'=>3,
            'manuf_name'=>'Epiphone'
        ]);
        Manufacturer::create([
            'id'=>4,
            'manuf_name'=>'G&L'
        ]);
        Manufacturer::create([
            'id'=>5,
            'manuf_name'=>'Takamine'
        ]);
        Manufacturer::create([
            'id'=>6,
            'manuf_name'=>'Elixir'
        ]);
        Manufacturer::create([
            'id'=>7,
            'manuf_name'=>'Boss'
        ]);
        Manufacturer::create([
            'id'=>8,
            'manuf_name'=>'Martin'
        ]);
        Manufacturer::create([
            'id'=>9,
            'manuf_name'=>'DAddarrio'
        ]);
        Manufacturer::create([
            'id'=>10,
            'manuf_name'=>'Ernie Ball'
        ]);
        Manufacturer::create([
            'id'=>11,
            'manuf_name'=>'Ibanez'
        ]);
    }
}
