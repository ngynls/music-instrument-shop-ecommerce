<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->delete();
        Category::create([
            'id'=>1,
            'category_name'=>'Acoustic Guitar'
        ]);
        Category::create([
            'id'=>2,
            'category_name'=>'Electric Guitar'
        ]);
        Category::create([
            'id'=>3,
            'category_name'=>'Acoustic Bass Guitar'
        ]);
        Category::create([
            'id'=>4,
            'category_name'=>'Acoustic-Electric Guitar'
        ]);
        Category::create([
            'id'=>5,
            'category_name'=>'Bass Guitar'
        ]);
        Category::create([
            'id'=>6,
            'category_name'=>'Acoustic Amp'
        ]);
        Category::create([
            'id'=>7,
            'category_name'=>'Guitar Combo'
        ]);
        Category::create([
            'id'=>8,
            'category_name'=>'Bass Combo'
        ]);
        Category::create([
            'id'=>9,
            'category_name'=>'Acoustic Guitar Strings'
        ]);
        Category::create([
            'id'=>10,
            'category_name'=>'Electric Guitar Strings'
        ]);
        Category::create([
            'id'=>11,
            'category_name'=>'Bass Guitar Strings'
        ]);
        Category::create([
            'id'=>12,
            'category_name'=>'Guitar Effects'
        ]);
    }
}
