<?php

use App\Product;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->delete();
        Product::create([
            'id'=>1,
            'product_name'=>'Epiphone Les Paul Special II Plus Limited Edition Electric Guitar Transparent Black',
            'price'=>326.36,
            'description'=>'Since its introduction almost 20 years ago, The Special-II has met the needs of countless musicians with its combination of Les Paul features, great tone and affordability. The Special II Plus Top Limited Edition Electric Guitar is made with a flamed maple top, mahogany body, bolt-on mahogany neck, smooth 22-fret rosewood fingerboard, and is every bit as handsome as its uptown cousins.
            Providing the sizzle and snarl that you can only get from humbuckers is a 650R (neck) and 700T (bridge) pickup combo. You&#39;ll be amazed how these U.S.A. designed pickups with their high-output magnets add more highs with increased definition and no muddiness. Slightly over-wound, the hotter 700T bridge pickup is smooth but can give you a little extra scream or growl when you need it while never losing its rich combination of enhanced lows and crystal clear highs.',
            'qtyInStock'=>10,
            'picture'=>'https://media.musiciansfriend.com/is/image/MMGS7/Les-Paul-Special-II-Plus-Limited-Edition-Electric-Guitar-Transparent-Black/430855000011000-00-500x500.jpg',
            'category_id'=>2,
            'manuf_id'=>3,
        ]);
        Product::create([
            'id'=>2,
            'product_name'=>'Gibson Les Paul Traditional 2019 Electric Guitar',
            'price'=>3989.05,
            'description'=>'Built to iconic specs, the Les Paul Traditional carries the vibe and history of one of the world&#39;s most popular electric guitars right up front. From the classic mahogany/maple top construction and nitrocellulose lacquer sunburst finish to the comfortably rounded neck and pair of punchy humbuckers, its everything you&#39;ve ever expected a Les Paul to be. Includes hardshell case.',
            'qtyInStock'=>10,
            'picture'=>'https://media.guitarcenter.com/is/image/MMGS7/Les-Paul-Traditional-2019-Electric-Guitar-Tobacco-Burst/L29431000001000-00-500x500.jpg',
            'category_id'=>2,
            'manuf_id'=>2,
        ]);
        Product::create([
            'id'=>3,
            'product_name'=>'G&L Tribute Legacy Electric Guitar Irish Ale',
            'price'=>711.16,
            'description'=>'The G&L Legacy is aptly named, blending contemporary refinements into Leo Fender&#39;s original G&L designs for  the S-500 and Comanche for an end result that delivers modern playability with vintage credibility. Warm, Alnico V single-coil pickups, refined by G&L&#39;s VP of engineering, Paul Gagon, to capture the best of vintage tones, speak with an expressive voice, and the gorgeous limited-availability Irish Ale finish makes this an instrument that will always stand out as a special breed, indeed.',
            'qtyInStock'=>10,
            'picture'=>'https://media.musiciansfriend.com/is/image/MMGS7/Tribute-Legacy-Electric-Guitar-Irish-Ale/L10442000001000-00-500x500.jpg',
            'category_id'=>2,
            'manuf_id'=>4,
        ]);
        Product::create([
            'id'=>4,
            'product_name'=>'Takamine GD-30CE 12-String Acoustic-Electric Guitar Wine Red',
            'price'=>641.31,
            'description'=>'This special edition GD30CE-12 dreadnought is a stage-worthy 12-string acoustic-electric guitar with a solid-top construction, soft Venetian-style cutaway, superior-sounding Takamine electronics system and an exclusive Wine Red finish. This gorgeous performance-ready model is crafted with a solid spruce top paired with a mahogany body that combine for a rich, resonant sound.
            The GD30CE-12 also features a mahogany neck with a 12&#34;-radius ovangkol fingerboard for great feel and playability. The onboard Takamine TP-4TD preamp system makes it easy to find and keep your tone with a built-in tuner and 3-band EQ with gain controls for excellent amplified performance and versatility.
            Rounding out the GD30CE-12 are a rosewood bridge, synthetic bone nut and split bridge saddle, rosewood headcap, pearloid rosette and dot inlays, and chrome die-cast tuners.',
            'qtyInStock'=>10,
            'picture'=>'https://media.musiciansfriend.com/is/image/MMGS7/GD-30CE-12-String-Acoustic-Electric-Guitar-Wine-Red/L18559000001000-00-500x500.jpg',
            'category_id'=>4,
            'manuf_id'=>5,
        ]);
        Product::create([
            'id'=>5,
            'product_name'=>'Elixir Electric Guitar Strings with NANOWEB Coating, Light (.010-.046)',
            'price'=>16.74,
            'description'=>'Elixir Strings with NANOWEB coating last longer and sound great!
            Amp up your guitar’s tone life with the same premium electric guitar strings that experienced players worldwide trust to keep their incredible tone longer than any other string available. Unlike other string brands, Elixir Strings coats the whole string. This protects not only the outer string surface but also the gaps between the windings where common tone-deadening gunk typically builds up in other brands’ coated and uncoated guitar strings.',
            'qtyInStock'=>20,
            'picture'=>'https://media.guitarcenter.com/is/image/MMGS7/Electric-Guitar-Strings-with-NANOWEB-Coating-Light-.010-.046/101536000000000-00-500x500.jpg',
            'category_id'=>10,
            'manuf_id'=>6,
        ]);
        Product::create([
            'id'=>6,
            'product_name'=>'Epiphone EB-0 Electric Bass Cherry',
            'price'=>374.99,
            'description'=>'Authorized by Gibson, with the EB-0 Electric Bass, Epiphone has brought back a classic bass guitar of the early 60s. Based on the SG, this guitar-sized (30-1/2" scale) electric bass is a great axe for anyone not quite large enough for a full-size bass. Though small and light, the Epiphone EB-0 Bass has huge sound, and full-size people can play it too. Case sold separately.',
            'qtyInStock'=>10,
            'picture'=>'https://media.guitarcenter.com/is/image/MMGS7/EB-0-Electric-Bass-Cherry/518523000019000-00-500x500.jpg',
            'category_id'=>5,
            'manuf_id'=>3,
        ]);
        Product::create([
            'id'=>7,
            'product_name'=>'Fender Mustang I V.2 20W 1x8 Guitar Combo Amp Black',
            'price'=>167.49,
            'description'=>'The Fender Mustang I V2 guitar combo amp adds new features to one of the best-selling amp series in the world. Get the flexibility you&#39;ve come to expect from a Mustang. The V.2 series raises the standard for versatility and muscle. Featuring five new amp models, five new effects and intelligent pitch shifting. The series features USB connectivity and FUSE software, letting your musical creativity and imagination run wild.',
            'qtyInStock'=>10,
            'picture'=>'https://media.guitarcenter.com/is/image/MMGS7/Mustang-I-V.2-20W-1x8-Guitar-Combo-Amp-Black/H91524000001000-00-500x500.jpg',
            'category_id'=>7,
            'manuf_id'=>1,
        ]);
        Product::create([
            'id'=>8,
            'product_name'=>'Boss DS-1 Distortion Pedal',
            'price'=>49.99,
            'description'=>'From screaming loud to whisper soft, the Boss DS-1 Distortion Pedal can faithfully reproduce the dynamics of your playing style. Level and distortion controls give you complete command of the amount of signal processing. Tone knob lets you tailor EQ right on the unit. The Boss DS-1 Distortion Pedal is a tough stompbox that can take whatever your boot can dish out. Includes 5-year warranty.',
            'qtyInStock'=>30,
            'picture'=>'https://media.guitarcenter.com/is/image/MMGS7/DS-1-Distortion-Pedal/151258000000000-00-500x500.jpg',
            'category_id'=>12,
            'manuf_id'=>7,
        ]);
        Product::create([
            'id'=>9,
            'product_name'=>'Fender Special Edition Standard Stratocaster Electric Guitar Black',
            'price'=>599.99,
            'description'=>'The Fender Special Edition Standard Stratocaster electric guitar is a modified version of a Standard Series Strat featuring custom upgraded appointments such as a tinted and gloss finished vintage-style maple neck, classic 50&#39; spaghetti logo, aged white plastic parts, a black/white/black three-ply pickguard and three classic Fender single-coil pickups. Other features include an alder body, 21-fret 9.5" radius fingerboard, and a classic Fender Synchronized Tremolo. Case sold separately.',
            'qtyInStock'=>10,
            'picture'=>'https://media.musiciansfriend.com/is/image/MMGS7/Special-Edition-Standard-Stratocaster-Electric-Guitar-Black/H64303000001000-00-500x500.jpg',
            'category_id'=>2,
            'manuf_id'=>1,
        ]);
        Product::create([
            'id'=>10,
            'product_name'=>'Martin DC Performing Artist Enhanced USA-Made Dreadnought Acoustic-Electric Guitar Sunburst',
            'price'=>1099.99,
            'description'=>'The Martin&#39;s DC Special Performing Artist Enhanced Dreadnought acoustic-electric guitar features a blend of the legendary Martin tone with solid-wood construction and elegant appointments. This instrument was designed for the player searching for a fine acoustic guitar that is attractive, and versatile enough to cover a variety of musical styles and performance techniques.
            The DC Special Performing Artist Enhanced Dreadnought acoustic-electric guitar comes with a deluxe gig bag.',
            'qtyInStock'=>5,
            'picture'=>'https://media.guitarcenter.com/is/image/MMGS7/DC-Performing-Artist-Enhanced-USA-Made-Dreadnought-Acoustic-Electric-Guitar-Sunburst/L10443000001000-00-500x500.jpg',
            'category_id'=>4,
            'manuf_id'=>8,
        ]);
        Product::create([
            'id'=>11,
            'product_name'=>'Fender Rumble 100 1x12 100W Bass Combo Amp',
            'price'=>299.99,
            'description'=>'The Rumble Series is a mighty leap forward in the evolution of portable bass amps.
            The Rumble 100W 1x12" bass combo is an ideal choice for practice, studio or rehearsal, with its great tone and easy-to-use controls. Features include an aux. input, XLR line out, FX loop and four-band EQ.
            Re-engineered from the ground up, Rumble amps are lighter and louder than ever, with even more power and a classic Fender vibe. A newly developed foot-switchable overdrive circuit, versatile three-button voicing palette and upgraded Eminence speaker, deliver powerful tones ideal for any gig.',
            'qtyInStock'=>5,
            'picture'=>'https://media.musiciansfriend.com/is/image/MMGS7/Rumble-100-1x12-100W-Bass-Combo-Amp/J06156000000000-00-500x500.jpg',
            'category_id'=>8,
            'manuf_id'=>2,
        ]);
        Product::create([
            'id'=>12,
            'product_name'=>'Boss Acoustic Singer Pro 120W 1x8 Acoustic Guitar Combo Amplifier',
            'price'=>549.99,
            'description'=>'The ACS Pro takes the acoustic stage amp to new levels of sound quality and creative versatility. Incorporating BOSS’s latest advancements in amplifier research, this compact powerhouse produces rich, vibrant sound with unmatched punch and clarity. Independent channels are provided for acoustic guitar and vocal, and each is equipped with discrete analog input circuits and three-band EQ for studio-quality sound.
            And with additional features such as Acoustic Resonance, looping, automatic vocal harmonies, effects, and much more, the ACS Pro gives acoustic musicians everything they need to deliver inspiring and impactful live performances.',
            'qtyInStock'=>5,
            'picture'=>'https://media.musiciansfriend.com/is/image/MMGS7/Acoustic-Singer-Pro-120W-1x8-Acoustic-Guitar-Combo-Amplifier/J50393000000000-00-500x500.jpg',
            'category_id'=>6,
            'manuf_id'=>7,
        ]);
        Product::create([
            'id'=>13,
            'product_name'=>'D&#39;Addario EJ16-3D Phosphor Bronze Light Acoustic Guitar Strings (3-Pack)',
            'price'=>18.89,
            'description'=>'These acoustic guitar strings feature a computer-controlled wrapping around a hex core. ',
            'qtyInStock'=>20,
            'picture'=>'https://media.musiciansfriend.com/is/image/MMGS7/EJ16-3D-Phosphor-Bronze-Light-Acoustic-Guitar-Strings-3-Pack/H71086000000000-00-500x500.jpg',
            'category_id'=>9,
            'manuf_id'=>9,
        ]);
        Product::create([
            'id'=>14,
            'product_name'=>'Ernie Ball 2832 Regular Slinky Roundwound Bass Strings',
            'price'=>16.99,
            'description'=>'Ernie Ball 2832 Regular Slinky Roundwound bass strings are roundwound for maximum clarity and serious punch. The list of endorsers for Ernie Ball Slinky bass strings reads like a who&#39;s who of modern rock bass. Made in the USA with specially formulated alloys for long life and maximum vibrancy. ',
            'qtyInStock'=>20,
            'picture'=>'https://images-na.ssl-images-amazon.com/images/I/713eN3HLETL._SX466_.jpg',
            'category_id'=>11,
            'manuf_id'=>10,
        ]);
        Product::create([
            'id'=>15,
            'product_name'=>'Ibanez PNB14E Parlor Acoustic-Electric Bass Guitar Natural',
            'price'=>249.99,
            'description'=>'The PNB14E Parlor acoustic-electric bass guitar is a short scale acoustic bass guitar that utilizes a parlor body shape. This compact instrument is ideal for situations such as playing on the couch, outdoors, as well as more informal occasions. However, the PBN14E Parlor acoustic-electric bass is also a valuable onstage tool. Since the 24.7" short scale is similar in feel to a regular guitar, it is more comfortable for both guitarists and electric bassists to play than a full-size acoustic bass.
            The shorter scale and compact parlor body also make it a more user-friendly platform for beginners.',
            'qtyInStock'=>10,
            'picture'=>'https://media.guitarcenter.com/is/image/MMGS7/PNB14E-Parlor-Acoustic-Electric-Bass-Guitar-Natural/K58564000001000-00-500x500.jpg',
            'category_id'=>3,
            'manuf_id'=>11,
        ]);

    }
}
