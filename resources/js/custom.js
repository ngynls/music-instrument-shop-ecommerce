/**
 * Toggles a black background color for sticky nav
 */
$(document).ready(function(){
    $(".fixedNavToggler").on("click", function(){
      $("nav").toggleClass('blackBg');
    });
  });

  $(window).scroll(function(){
    $('nav').toggleClass('scrolled', $(this).scrollTop()>0);
  });

  /**
   * ScrollReveal
   */
  window.sr=ScrollReveal();
  sr.reveal('#slogan',{
    duration:2500,
    origin:'bottom'
  });
  sr.reveal('#featured1',{
    duration:3000,
    origin:'top'
  });
  sr.reveal('#featured2',{
    duration:3000,
    origin:'top',
    delay:500
  });
  sr.reveal('#featured3',{
    duration:3000,
    origin:'top',
    delay:1000
  });
  sr.reveal('#featured4',{
    duration:3000,
    origin:'top',
    delay:1500
  });
  sr.reveal('#contact',{
    duration:3000,
    origin:'bottom'
  });

  /**
   * Toggles sorting
   */
  $(document).ready(function(){
    $("#sorter").on("change",function(){
        let option=$("#sorter").val();
        console.log(option);
        window.location=option;
      });
  });
