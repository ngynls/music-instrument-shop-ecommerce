@extends('layouts.app')

@section('page-title')
<section class="pageTitle">
        <div id="pageTitle">
            <div class="text-light" id="currentPageLabel">
                <h1>Order Details</h1>
            </div>
        </div>
</section>
@endsection

@section('content')
<div class="container">
    <h2>Order</h2>
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Order Date</th>
                        <th scope="col">Order Number</th>
                        <th scope="col">Order Total</th>
                        <th scope="col">Status</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{$order->created_at}}</td>
                        <td>{{$order->id}}</td>
                        <td>${{number_format($order->orderTotal,2)}}</td>
                        <td>{{$order->status}}</td>
                    </tr>
                </tbody>
            </table>
    </div>
    <h2>Billing Information</h2>
    <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Customer First Name</th>
                        <th scope="col">Customer Last Name</th>
                        <th scope="col">Address</th>
                        <th scope="col">City</th>
                        <th scope="col">State</th>
                        <th scope="col">Zip Code</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{$order->customerFirstName}}</td>
                        <td>{{$order->customerLastName}}</td>
                        <td>{{$order->address}}</td>
                        <td>{{$order->city}}</td>
                        <td>{{$order->state}}</td>
                        <td>{{$order->zipcode}}</td>
                    </tr>
                </tbody>
            </table>
    <h2>Items ordered</h2>
    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">Name</th>
                    <th scope="col">Qty</th>
                    <th scope="col">Price</th>
                    <th scope="col">Tax</th>
                    <th scope="col">Total</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($cartSummary as $rowId=>$product)
                    <tr>
                        <td><a href="/products/{{$product["id"]}}">{{htmlspecialchars_decode($product["name"], ENT_QUOTES)}}</a></td>
                        <td>{{$product["qty"]}}</td>
                        <td>${{$product["price"]}}</td>
                        <td>${{number_format($product["tax"],2)}}</td>
                        <td>${{number_format($product["subtotal"]+$product["tax"],2)}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
