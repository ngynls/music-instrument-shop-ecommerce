@extends('layouts.app')

@section('page-title')
<section class="pageTitle">
        <div id="pageTitle">
            <div class="text-light" id="currentPageLabel">
            <h1>Search Results</h1>
            </div>
        </div>
</section>
@endsection

@section('content')
<div class="container">
  <h1>Search results for "{{request()->search}}"</h1>
  <div class="row">
    <div class="col-xl-6 mb-3">
        <form id="sort-options" action="" method="GET">
            @csrf
            <select id="sorter" name="sorter">
                <option value="{{route('search',['search'=>request()->search,'sort'=>'byNewest'])}}">Sort by Newest</option>
                <option value="{{route('search',['search'=>request()->search,'sort'=>'byPriceAsc'])}}">Sort by Price (low to high)</option>
                <option value="{{route('search',['search'=>request()->search,'sort'=>'byPriceDesc'])}}">Sort by Price (high to low)</option>
                <option value="{{route('search',['search'=>request()->search,'sort'=>'byNameAsc'])}}">Sort by Name (a-z)</option>
                <option value="{{route('search',['search'=>request()->search,'sort'=>'byNameDesc'])}}">Sort by Name (z-a)</option>
            </select>
        </form>
    </div>
</div>
  <div class="row">
  @if(count($results)>0)
    @foreach($results as $result)
    <div class="card">
<article class="itemlist">
	<div class="row">
		<aside class="col-lg-3 col-md-12 col-sm-12 pt-3">
			<div class="d-flex justify-content-center"><a href="/products/{{$result->id}}"><img class="img-md" src="{{$result->picture}}" width="200" height="200"></a></div>
		</aside> <!-- col.// -->
		<div class="col-lg-6 col-md-12 col-sm-12 tm-20 pt-3">
			<div class="pl-15 pr-15 info-block">
				<h4 class="title"> <a href="/products/{{$result->id}}">{{$result->product_name}}</a> </h4>
				<p> {{htmlspecialchars_decode($result->description, ENT_QUOTES)}} </p>
			</div> <!-- text-wrap.// -->
		</div> <!-- col.// -->
		<aside class="col-lg-3 col-md-12 col-sm-12 tm-20 pt-3 price-block">
			<div class="border-left pl-3">
				<div class="price-wrap">
					<span class="h3 price">$ {{$result->price}} </span>
				</div> <!-- info-price-detail // -->
				<p class="text-success">Free shipping</p>
				<p>
        </p>
			</div> <!-- action-wrap.// -->
		</aside> <!-- col.// -->
	</div> <!-- row.// -->
</article> <!-- itemlist.// -->
    @endforeach
    {{ $results->appends(request()->except('page'))->links() }}
  </div>
  @else
    <p>No products found</p>
  @endif
</div>
@endsection
