@extends('layouts.app')

@section('page-title')
<section class="pageTitle">
        <div id="pageTitle">
            <div class="text-light" id="currentPageLabel">
                <h1>My Account</h1>
            </div>
        </div>
</section>
@endsection

@section('content')
<div class="container">
    <div class="row mb-30">
        <div class="col">
        <h2>Welcome {{Auth::user()->email}}</h2>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <h4>Recent orders</h4>
            <a href="/myOrderHistory">View all order history</a>
            <div class="table-responsive">
                    <table class="table">
                      <thead>
                          <tr>
                              <th>Order Date</th>
                              <th>Order Number</th>
                              <th>Order Total</th>
                              <th>Status</th>
                              <th></th>
                          </tr>
                      </thead>
                      <tbody>
                          @foreach($recentOrders as $order)
                          <tr>
                            <td>{{$order->created_at}}</td>
                            <td>{{$order->id}}</td>
                            <td>${{number_format($order->orderTotal,2)}}</td>
                            <td>{{$order->status}}</td>
                            <td><a href="{{route('viewOrder',$order->id)}}">View details</a></td>
                          </tr>
                          @endforeach
                      </tbody>
                    </table>
            </div>
        </div>
    </div>
</div>
@endsection
