@extends('layouts.app')

@section('page-title')
<section class="pageTitle">
        <div id="pageTitle">
            <div class="text-light" id="currentPageLabel">
                <h1>Checkout</h1>
            </div>
        </div>
</section>
@endsection

@section('content')
<div class="container">
    <div class="row mb-30">
        <div class="col">
            <h2>Order successful</h2>
            <p>Your payment has been processed and your order is complete. We will be dispatching your order soon. You will receive an email confirmation shortly.</p>
            <a href="/products">Back to shopping</a>
        </div>
    </div>
</div>
@endsection
