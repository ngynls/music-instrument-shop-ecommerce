@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row border p-3">
        <div class="product-image col-lg-6 pb-3">
            <img src="{{$product->picture}}" alt='Picture not found'>
        </div>
        <div class="product-information border-left col-lg-6">
            <h1>{{htmlspecialchars_decode($product->product_name, ENT_QUOTES)}}</h1>
            <p><strong>${{$product->price}}</strong></p>
            <p>{{htmlspecialchars_decode($product->description, ENT_QUOTES)}}</p>
            @if($product->qtyInStock<1) <p><span class="text-danger">Out of stock</span> </p>
                @else
                <p><span class="text-success">In stock:</span> {{$product->qtyInStock}} left!</p>
                <form action="{{route('cart.index')}}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="productId" value="{{$product->id}}">
                    <input type="hidden" name="name" value="{{$product->product_name}}">
                    <input type="hidden" name="price" value="{{$product->price}}">
                    <label>Qty: </label>
                    <input type="number" name="qty" min="1" max="50" required>
                    <button class="btn btn-dark">Add to cart</button>
                </form>
                @endif
        </div>
    </div>
</div>
@endsection
