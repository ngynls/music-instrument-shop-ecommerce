@extends('layouts.app')

@section('page-title')
<section class="pageTitle">
    <div id="pageTitle">
        <div class="text-light" id="currentPageLabel">
            <h1>Checkout</h1>
        </div>
    </div>
</section>
@endsection

@section('content')
<div class="container">
    @if(isset($errorMsg))
    <div class="alert alert-warning" role="alert">
        {{$errorMsg}}
    </div>
    @endif
    <div class="row">
        <div class="col-lg-8">
            <form method="POST" action="{{ route('payment') }}">
                @csrf
                <h1>Billing Information</h1>
                <div class="form-group">
                    <label for="firstName">First Name</label>
                    <input class="form-control" type="text" name="firstName" required>
                </div>
                <div class="form-group">
                    <label for="lastName">Last Name</label>
                    <input class="form-control" type="text" name="lastName" required>
                </div>
                <div class="form-group">
                    <label for="address">Address</label>
                    <input class="form-control" type="text" name="address" required>
                </div>
                <div class="form-group">
                    <label for="country">Country</label>
                    <input class="form-control" type="text" name="country" required>
                </div>
                <div class="form-group">
                    <label for="city">City</label>
                    <input class="form-control" type="text" name="city" required>
                </div>
                <div class="form-group">
                    <label for="state">State/Province</label>
                    <input class="form-control" type="text" name="state" required>
                </div>
                <div class="form-group">
                    <label for="zipcode">Zip Code/Postal Code</label>
                    <input class="form-control" type="text" name="zipcode" required>
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input class="form-control" type="email" name="email" required>
                </div>
                <h1>Payment Options</h1>
                <div class="form-group">
                    <label for="nameoncard">Full Name On Card</label>
                    <input class="form-control" type="text" name="nameOnCard" required>
                </div>
                <div class="form-group">
                    <label for="cardNumb">Card Number</label>
                    <input class="form-control" type="text" name="cardNumb" required>
                </div>
                <div class="row">
                    <div class="form-group col-sm-7">
                        <label for="expirationDate">Expiration Date</label>
                        <div class="form-inline">
                            <input class="form-control mr-4" type="number" name="expMonth" id="expFieldMonth" min="1" max="12"
                                placeholder="MM" required>
                            <input class="form-control" type="number" name="expYear" id="expFieldYear" min="2018" max="3000"
                                placeholder="YYYY" required>
                        </div>
                    </div>
                    <div class="form-group col-sm-3">
                        <label for="cvc">CVC</label>
                        <div class="form-inline">
                            <input class="form-control" type="number" name="cvc" pattern="[0-9]{3}" required>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-dark mt-2 mb-3">Complete Order</button>
            </form>
        </div>
        <div class="col-lg-4">
            <h1>Cart Summary</h1>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Qty</th>
                            <th scope="col">Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach(Cart::content() as $item)
                        <tr>
                            <td><a href="/products/{{$item->model->id}}">{{$item->name}}</a></td>
                            <td>{{$item->qty}}</td>
                            <td>${{$item->price}}</td>
                        </tr>
                        @endforeach
                        <tr>
                            <td colspan="4"><span class="bold"> Subtotal: </span> ${{Cart::subtotal()}}</td>
                        </tr>
                        </tr>
                        <td colspan="4"><span class="bold">Tax: </span> ${{Cart::tax()}}</td>
                        </tr>
                        <tr>
                            <td colspan="4"><span class="bold">Total: </span>${{Cart::total()}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
