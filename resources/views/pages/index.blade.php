<!DOCTYPE html>
<html lang="eng">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, width=device-width" />
	<title>Musician's Center</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Righteous" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=ZCOOL+QingKe+HuangYou" rel="stylesheet">
</head>
<body>
    <header>
    @include('inc.navbar')
              <section id="slogan">
                <div class="text-light">
                    <h1>Become the rockstar you always wanted to be</h1>
                    <h3>Your #1 destination for the best musical instruments, gear and exclusive content to help you get the sound you’re after</h3>
                    <a href="/products"><button class="btn btn-outline-light" id="shopNowBtn">Shop now</button></a>
                </div>
              </section>
    </header>
    <div class="container">
      <section class="mainContent" id="featuredProducts">
          <h1>Featured products</h1>
          <div class="featuredProductsContainer">
              <div class="row">
                  <div class="col-lg-3">
                      <div class="featuredElement" id="featured1">
                          <a href="/products/1"><img class="featuredImg" src="{{URL::asset('/imgs/guitar1.jpg')}}" alt="guitar 1"/></a>
                          <p class="featuredProductTitle">Epiphone Les Paul Special II Plus</p>
                          <p class="featuredProductPrice">CAD 326.36</p>
                      </div>
                  </div>
                  <div class="col-lg-3">
                        <div class="featuredElement" id="featured2">
                            <a href="/products/2"><img class="featuredImg" src="{{URL::asset('/imgs/guitar2.jpg')}}" alt="guitar 2"/></a>
                            <p class="featuredProductTitle">Gibson Les Paul Traditional 2019</p>
                            <p class="featuredProductPrice">CAD 3,989.05</p>
                        </div>
                  </div>
                  <div class="col-lg-3">
                        <div class="featuredElement" id="featured3">
                            <a href="products/3"><img class="featuredImg" src="{{URL::asset('/imgs/guitar3.jpg')}}" alt="guitar 3"/></a>
                            <p class="featuredProductTitle">G&L Tribute Legacy</p>
                            <p class="featuredProductPrice">CAD 711.16</p>
                        </div>
                  </div>
                  <div class="col-lg-3">
                        <div class="featuredElement" id="featured4">
                            <a href="products/4"><img class="featuredImg" src="{{URL::asset('/imgs/guitar4.jpg')}}" alt="guitar 4"/></a>
                            <p class="featuredProductTitle">Takamine GD-30CE 12-String</p>
                            <p class="featuredProductPrice">CAD 641.31</p>
                        </div>
                  </div>
              </div>
          </div>
      </section>
      <section class="mainContent" id="contact">
          <h1>Newsletter</h1>
          <form action="{{route('subscribe')}}" method="POST" id="newsletterForm">
              @csrf
              <input type="email" name="email" id="subscriptionBox" placeholder="email address" required>
              <button type="submit" value="Subscribe" name="subscribe">Subscribe</button>
          </form>
      </section>
    </div>
    @include('inc.footer')
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/scrollreveal"></script>
    <script type="text/javascript" src="{{ asset('js/custom.js') }}"></script>
</body>
</html>
