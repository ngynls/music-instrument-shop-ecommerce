@extends('layouts.app')

@section('page-title')
<section class="pageTitle">
    <div id="pageTitle">
        <div class="text-light" id="currentPageLabel">
            <h1>Newsletter Subscription</h1>
        </div>
    </div>
</section>
@endsection

@section('content')
<div class="container">
    <div class="row mb-30">
        <div class="col">
            <h2>Subscription successful</h2>
            <p>Thank you for subscribing to our newsletter!</p>
            <a href="/">Back to landing page</a>
        </div>
    </div>
</div>
@endsection
