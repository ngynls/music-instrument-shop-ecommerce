@extends('layouts.app')

@section('page-title')
<section class="pageTitle">
        <div id="pageTitle">
            <div class="text-light" id="currentPageLabel">
                <h1>Cart</h1>
            </div>
        </div>
</section>
@endsection

@section('content')
<div class="container">
    <h1>Shopping Cart</h1>
    @if(isset($successMsg))
    <div class="alert alert-success" role="alert">
        {{$successMsg}}
    </div>
    @endif
    @if(isset($errorMsg))
    <div class="alert alert-warning" role="alert">
        {{$errorMsg}}
    </div>
    @endif
    @if(Cart::count()>0)
    <h2>{{Cart::count()}} item(s) in your cart</h2>
    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col"></th>
                    <th scope="col">Name</th>
                    <th scope="col">Qty</th>
                    <th scope="col">Price</th>
                    <th scope="col">&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                @foreach(Cart::content() as $item)
                <tr>
                <td><a href="/products/{{$item->model->id}}"><img src="{{$item->model->picture}}" alt="{{$item->name}}" width="136px" height="136px"></a></td>
                    <td><a href="/products/{{$item->model->id}}">{{$item->name}}</a></td>
                    <td>
                        <form action="{{route('cart.update',$item->rowId)}}" method="POST">
                            {{ csrf_field() }}
                            {{method_field("PATCH")}}
                            <input type="hidden" name="id" value="{{$item->id}}">
                            <input class="mt-1" type="number" name="qty" value="{{$item->qty}}" min="1" max="5" id="qty{{$item->id}}">
                            <button class="btn btn-dark mt-1" type="submit">
                                Update
                            </button>
                        </form>
                    </td>
                    <td>${{$item->price}}</td>
                    <td>
                        <form action="{{route('cart.destroy',$item->rowId)}}" method="POST">
                            {{ csrf_field() }}
                            {{method_field("DELETE")}}
                            <input type="hidden" name="id" value="{{$item->id}}">
                            <input type="hidden" name="qty" value="{{$item->qty}}">
                            <button class="btn btn-dark" type="submit">
                                <i class="fa fa-trash" aria-hidden="true" id="trash{{$item->id}}"></i>
                            </button>
                        </form>
                    </td>
                </tr>
                @endforeach
                <tr>
                    <td colspan="4"><span class="bold"> Subtotal: </span> ${{Cart::subtotal()}}</td>
                </tr>
                </tr>
                <td colspan="4"><span class="bold">Tax: </span> ${{Cart::tax()}}</td>
                </tr>
                <tr>
                    <td colspan="4"><span class="bold">Total: </span>${{Cart::total()}}</td>
                </tr>
            </tbody>
        </table>
    </div>
    <a href="{{url('/products')}}"><button class="btn btn-dark">Back to shopping </button></a>
    <a href="{{url('/checkout')}}"><button class="btn btn-dark mt-2">Checkout</button></a>
    @else
    <h2>0 item in your cart</h2>
    @endif
</div>
@endsection
