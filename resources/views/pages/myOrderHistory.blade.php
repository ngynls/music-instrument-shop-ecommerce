@extends('layouts.app')

@section('page-title')
<section class="pageTitle">
        <div id="pageTitle">
            <div class="text-light" id="currentPageLabel">
                <h1>My Order History</h1>
            </div>
        </div>
</section>
@endsection

@section('content')
<div class="container">
    <div class="row mb-30">
        <div class="col">
            <h4>My Orders</h4>
            <div class="table-responsive">
                    <table class="table">
                      <thead>
                          <tr>
                              <th>Order Date</th>
                              <th>Order Number</th>
                              <th>Order Total</th>
                              <th>Status</th>
                              <th></th>
                          </tr>
                      </thead>
                      <tbody>
                          @foreach($orderHistory as $order)
                          <tr>
                            <td>{{$order->created_at}}</td>
                            <td>{{$order->id}}</td>
                            <td>${{number_format($order->orderTotal,2)}}</td>
                            <td>{{$order->status}}</td>
                            <td><a href="{{route('viewOrder',$order->id)}}">View details</a></td>
                          </tr>
                          @endforeach
                      </tbody>
                    </table>
            </div>
        </div>
    </div>
</div>
@endsection
