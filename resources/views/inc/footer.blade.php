<footer class="blackBg">
    <div class="footerArea">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-5">
                    <div class="footerElement">
                        <h3>About us</h3>
                        <p>Sorem ipsum dolor sit amet consadip eisicing elit sed do eiusmod tempor incididunt labore et
                            dolore magna aliqua. Ut enim ad minim veniam quis nostrud exercitation</p>
                        <div class="socialMediaSection">
                            <a class="socialMediaIcon" href="#"><i class="fab fa-facebook"></i></a>
                            <a class="socialMediaIcon" href="#"><i class="fab fa-twitter"></i></a>
                            <a class="socialMediaIcon" href="#"><i class="fab fa-linkedin"></i></a>
                            <a class="socialMediaIcon" href="#"><i class="fab fa-instagram"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3">
                    <div class="footerElement">
                        <h3>Quick Link</h3>
                        <div class="footerLink">
                            <a href="#">Store Location</a>
                            @auth
                            <a href="/myOrderHistory">Order Tracking</a>
                            @else
                            <a href="/login">Order Tracking</a>
                            @endauth
                            @auth
                            <a href="/myAccount">My account</a>
                            @else
                            <a href="/login">My account</a>
                            @endauth
                            <a href="#">About us</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3">
                    <div class="footerElement">
                        <h3>My account</h3>
                        <div class="myAccountLink">
                            @auth
                            <a href="/myAccount">My account</a>
                            @else
                            <a href="/login">My account</a>
                            @endauth
                            <a href="/checkout">Checkout</a>
                            <a href="/cart">Shopping cart</a>
                            <a href="#">Wishlist</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="footerElement">
                        <h3>Contact us</h3>
                        <div class="contactInfo">
                            <p><i class="fas fa-phone"></i> +0123456789</p>
                            <p><i class="fas fa-map-marker-alt"></i> 1000 Gotham Ave, Houston, TX, USA</p>
                            <p><i class="fas fa-envelope"></i> support@gmail.com</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyrightArea copyrightBorder">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="copyrightSection">
                        <p>
                            Copyright © 2018. All Rights Reserved
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
