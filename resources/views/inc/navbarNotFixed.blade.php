<section class="blackBg">
    <nav class="navbar navbar-expand-lg navbar-dark">
        <a class="navbar-brand" href="/">Musician's Center</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerNotFixed" aria-controls="navbarTogglerDemo02"
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarTogglerNotFixed">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item">
                    <a class="nav-link" href="/products">Shop</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Blog</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/cart">Cart</a>
                </li>
                @auth
                <li class="nav-item">
                    <a class="nav-link" href="/myAccount">My account</a>
                </li>
                <li class="nav-item">
                        <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">Logout</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                </li>
                @else
                <li class="nav-item">
                        <a class="nav-link" href="/login">Login</a>
                </li>
                @endauth
                <li class="nav-item">
                    <a class="nav-link" href="#">Contact Us</a>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                    <li class="nav-item">
                    <form action="{{route('search')}}" id="searchBar" method="GET" class="form-inline my-2 my-lg-0">
                                    @csrf
                                    <input class="form-control mr-sm-2" name="search" type="search" placeholder="Search" aria-label="Search" required>
                                    <button class="btn my-2 my-sm-0" type="submit" id="searchButton">Search</button>
                            </form>
                    </li>
            </ul>
        </div>
    </nav>
</section>
