<nav class="navbar fixed-top navbar-expand-lg navbar-dark">
    <a class="navbar-brand" href="/">Musician's Center</a>
    <button class="navbar-toggler fixedNavToggler" type="button" data-toggle="collapse" data-target="#navbarToggler" aria-controls="navbarTogglerDemo01"
        aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarToggler">
        <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
            <li class="nav-item">
                <a class="nav-link" href="/products">Shop</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Blog</a>
            </li>
            <li class="nav-item">
                    <a class="nav-link" href="/cart">Cart</a>
            </li>
            @auth
            <li class="nav-item">
                <a class="nav-link" href="/myAccount">My account</a>
            </li>
            @else
            <li class="nav-item">
                    <a class="nav-link" href="/login">Login</a>
            </li>
            @endauth
            <li class="nav-item">
                <a class="nav-link" href="#">Contact Us</a>
            </li>
        </ul>
    </div>
</nav>
