@extends('layouts.app')

@section('page-title')
<section class="pageTitle">
    <div id="pageTitle">
        <div class="text-light" id="currentPageLabel">
            <h1>Login</h1>
        </div>
    </div>
</section>
@endsection

@section('content')
<div id="loginContainer">
    <div class="loginArea">
        <div class="row">
            <div class="col-lg-2"></div>
            <div class="col-lg-4 mb-30" id="loginBox">
                <h2>Login</h2>
                <p>Please login using your account details</p>
                <div class="card">
                        <div class="card-body">
                            <form method="POST" action="{{ route('login') }}">
                                @csrf

                                <div class="form-group row">
                                    <label for="email" class="col-md-4 col-form-label text-md-right">Email address</label>

                                    <div class="col-md-6">
                                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                            name="email" value="{{ old('email') }}" required autofocus>

                                        @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                    <div class="col-md-6">
                                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                            name="password" required>

                                        @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-6 offset-md-4">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="remember" id="remember"
                                                {{ old('remember') ? 'checked' : '' }}>

                                            <label class="form-check-label" for="remember">
                                                {{ __('Remember Me') }}
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-8 offset-md-4">
                                        <button type="submit" class="btn" id="loginButton">
                                            {{ __('Login') }}
                                        </button>

                                        @if (Route::has('password.request'))
                                        <a class="btn btn-link" href="{{ route('password.request') }}">
                                            {{ __('Forgot Your Password?') }}
                                        </a>
                                        @endif
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
            </div>
            <div class="col-lg-1"></div>
            <div class="col-lg-4 mb-30" id="registerBox">
                <h2>Create account</h2>
                <p>Register your new account now & start shopping</p>
                <a href="{{route('register')}}">
                    <button class="btn" id="registerButton">Register</button>
                </a>
            </div>
            <div class="col-lg-1"></div>
        </div>
    </div>
</div>
@endsection
