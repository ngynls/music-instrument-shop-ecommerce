<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Manufacturer extends Model
{
    //Table name
    protected $table='manufacturers';
    public $primaryKey='id';
    public $timestamps=false;
}
