<?php

namespace App\Http\Controllers;

use App\Order;
use Cartalyst\Stripe\Stripe;
use Illuminate\Http\Request;
use Gloudemans\Shoppingcart\Facades\Cart;

class CheckoutController extends Controller
{
    public function index(){
        if(Cart::count()<1)
            return redirect('/cart');
        else
            return view('pages.checkout');
    }

    public function payment(Request $request){
        $stripe=Stripe::make(env('STRIPE_SECRET'));
        try{
            $token=$stripe->tokens()->create([
                'card'=>[
                    'number' => $request->get('cardNumb'),
                    'exp_month' => $request->get('expMonth'),
                    'exp_year' => $request->get('expYear'),
                    'cvc' => $request->get('cvc')
                ]
            ]);
            if(!isset($token['id'])){
                $errorMsg="Payment cannot be processed";
                return redirect()->route('checkout')->with('errorMsg',$errorMsg);
            }
            $charge=$stripe->charges()->create([
                'source'=> $token['id'],
                'currency'=> 'CAD',
                'amount' => Cart::total()
            ]);
            if($charge['status']=='succeeded'){
                //Add order to database
                $order= new Order;
                $order->customerFirstName=$request->get('firstName');
                $order->customerLastName=$request->get('lastName');
                $order->address=$request->get('address');
                $order->country=$request->get('country');
                $order->city=$request->get('city');
                $order->state=$request->get('state');
                $order->zipcode=$request->get('zipcode');
                $order->customerEmail=$request->get('email');
                $order->cartContents=Cart::content()->toJson();
                $order->orderTotal=Cart::total();
                $order->status="Pending";
                $order->save();
                Cart::destroy();
                return view('pages.paymentSuccessful');
            }
            else{
                $errorMsg="Payment cannot be processed";
                return redirect()->route('checkout')->with('errorMsg',$errorMsg);
            }
        }
        catch(Exception $e){
            $errorMsg="Payment cannot be processed";
            return redirect()->route('checkout')->with('errorMsg',$errorMsg);
        }
        catch(\Cartalyst\Stripe\Exception\CardErrorException $e) {
            $errorMsg="[CardErrorException] Payment cannot be processed";
            return redirect()->route('checkout')->with('errorMsg',$errorMsg);
        }
    }
}
