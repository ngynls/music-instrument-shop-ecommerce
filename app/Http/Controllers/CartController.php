<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Gloudemans\Shoppingcart\Facades\Cart;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.cart');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $productInDB=Product::find($request->productId);
        if($request->qty<=$productInDB->qtyInStock){
            Cart::add($request->productId,$request->name, $request->qty, $request->price)->associate('App\Product');
            //Decrease qty in db
            $productInDB->qtyInStock-=$request->qty;
            $productInDB->save();
            $successMsg="Item was successfully added to cart";
            return view("pages.cart")->with('successMsg',$successMsg);
        }
        else{
            $errorMsg="[Error: insufficient stock] Item was not added to cart";
            return view("pages.cart")->with('errorMsg',$errorMsg);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $productInDB=Product::find($request->id);
        //re-add the number of items for the product that's in the cart to the qty in db
        $productInCart=Cart::get($id);
        $productInDB->qtyInStock+=$productInCart->qty;
        if($request->qty<=$productInDB->qtyInStock){
            //substract with the updated qty $request->qty
            $productInDB->qtyInStock-=$request->qty;
            $productInDB->save();
            Cart::update($id, $request->qty);
            $successMsg="Quantity for item was updated";
            return view("pages.cart")->with('successMsg',$successMsg);
        }
        else{
            $errorMsg="[Error: insufficient stock] Quantity for item was not updated";
            return view("pages.cart")->with('errorMsg',$errorMsg);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $product=Product::find($request->id);
        $product->qtyInStock+=$request->qty;
        $product->save();
        Cart::remove($id);
        $successMsg="Item was successfully removed from cart";
        return view("pages.cart")->with('successMsg', $successMsg);
    }
}
