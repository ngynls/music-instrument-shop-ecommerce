<?php

namespace App\Http\Controllers;


use App\Product;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function search(Request $request){

        if(request()->sort){
            switch(request()->sort){
                case "byNewest":
                    $results=Product::where('product_name',$request->input('search'))
                    ->orWhere('product_name', 'like', '%' . $request->input('search') . '%')->orderBy('created_at','desc')->paginate(3);
                    break;
                case "byPriceAsc":
                    $results=Product::where('product_name',$request->input('search'))
                    ->orWhere('product_name', 'like', '%' . $request->input('search') . '%')->orderBy('price','asc')->paginate(3);
                    break;
                case "byPriceDesc":
                    $results=Product::where('product_name',$request->input('search'))
                    ->orWhere('product_name', 'like', '%' . $request->input('search') . '%')->orderBy('price','desc')->paginate(3);
                    break;
                case "byNameAsc":
                    $results=Product::where('product_name',$request->input('search'))
                    ->orWhere('product_name', 'like', '%' . $request->input('search') . '%')->orderBy('product_name','asc')->paginate(3);
                    break;
                case "byNameDesc":
                    $results=Product::where('product_name',$request->input('search'))
                    ->orWhere('product_name', 'like', '%' . $request->input('search') . '%')->orderBy('product_name','desc')->paginate(3);
                    break;
                default:
                    $results=Product::where('product_name',$request->input('search'))
                    ->orWhere('product_name', 'like', '%' . $request->input('search') . '%')->paginate(3);
            }
        }
        else{
            $results=Product::where('product_name',$request->input('search'))
            ->orWhere('product_name', 'like', '%' . $request->input('search') . '%')->paginate(3);
        }
        return view('pages.searchResults')->with('results',$results);
    }
}
