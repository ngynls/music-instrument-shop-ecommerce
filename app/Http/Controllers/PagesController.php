<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PagesController extends Controller
{
    public function index(){
        return view('pages.index');
    }

    public function myAccount(){
        $recentOrders=Order::where('customerEmail',Auth::user()->email)->orderBy('created_at','desc')->take(3)->get();
        return view('pages.myAccount')->with('recentOrders',$recentOrders);
    }

    public function myOrderHistory(){
        $orderHistory=Order::where('customerEmail',Auth::user()->email)->orderBy('created_at','desc')->get();
        return view('pages.myOrderHistory')->with('orderHistory',$orderHistory);
    }

    public function viewOrder($id){
        $order=Order::find($id);
        $cartSummary=json_decode($order->cartContents,true);
        //dd($cartSummary);
        if($order->customerEmail == Auth::user()->email){
            return view('pages.orderDetails')->with(['order'=>$order,'cartSummary'=>$cartSummary]);
        }
    }
}
