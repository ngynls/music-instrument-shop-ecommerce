<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Newsletter;

class SubscriptionController extends Controller
{
    public function subscribe(Request $request){
        if(!Newsletter::isSubscribed($request->email)){
            Newsletter::subscribe($request->email);
            return view('pages.subscriptionSuccessful');
        }
        else {
            return view('pages.alreadySubscribed');
        }
    }
}
