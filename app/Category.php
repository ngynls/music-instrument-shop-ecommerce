<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //Table name
    protected $table='categories';
    public $primaryKey='id';
    public $timestamps=false;
}
