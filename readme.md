# Musician's Center
An e-commerce solution for a musical instrument shop using Laravel framework

## Technologies used:
* Laravel Framework
* Bootstrap 4
* Stripe-Laravel : https://cartalyst.com/manual/stripe-laravel/9.x
* LaravelShoppingCart: https://github.com/hardevine/LaravelShoppingcart
* ScrollReveal: https://github.com/scrollreveal/scrollreveal
* XAMPP
* MySQL
* JQuery
* Mailchimp

## Screenshots
![Screenshot #1](https://i.imgur.com/03HWQOq.png)
![Screenshot #2](https://i.imgur.com/fF1MNnP.png)
![Screenshot #3](https://i.imgur.com/FSyPhJ3.png)
![Screenshot #4](https://i.imgur.com/ak5scRE.png)

## How to run
* Clone the repository & cd into it https://bitbucket.org/ngynls/music-instrument-shop-ecommerce.git
* `composer install`
* `php artisan key:generate`
* Change the DB_DATABASE, DB_USERNAME, DB_PASSWORD, STRIPE_KEY, STRIPE_SECRET, MAILCHIMP_APIKEY & MAILCHIMP_LIST_ID fields with your own credentials
* `php artisan migrate --seed`
* `php artisan serve`
